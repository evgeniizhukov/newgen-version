
//TODO: Опишите, как можно отфильтровать список курсов, чтобы выдались только подходящие по цене?
// Реализуйте на JavaScript (или TypeScript) функцию, проводящую такую фильтрацию. 
// Список курсов

let courses = [
    { name: "Courses in England", prices: [0, 100] }, 
    { name: "Courses in Germany", prices: [500, null] }, 
    { name: "Courses in Italy", prices: [100, 200] }, 
    { name: "Courses in Russia", prices: [null, 400] },
    { name: "Courses in China", prices: [50, 250] },
    { name: "Courses in USA", prices: [200, null] },
    { name: "Courses in Kazakhstan", prices: [56, 324] },
    { name: "Courses in France", prices: [null, null] },
];

// Варианты цен (фильтры), которые ищет пользователь
let requiredRange1 = [null, 200];
let requiredRange2 = [100, 350];
let requiredRange3 = [200, null];

// Решение :

// Функция для сравнения диапазоны цент пользователя и курсов
const comparePrice = (firstPrice, secondPrice) => {
    let compareMinCost = false
    let compareMaxCost = false

    if (firstPrice[0]) {
        compareMinCost = secondPrice[0] ? firstPrice[0] <= secondPrice[0] : false
    } else {
        compareMinCost = true
    }

    if (firstPrice[1]) {
        compareMaxCost = secondPrice[1] ? firstPrice[1] >= secondPrice[1] : false
    } else {
        compareMaxCost = true
    }
    
    return compareMinCost && compareMaxCost
}

//Функция сравнения для сортировки курсов по минимальной стоимости
const compareFunction = (firstValue, secondValue) => {
    const a = firstValue.prices[0]
    const b = secondValue.prices[0]

    if (!a || a < b) {
        return -1;
    }

    if (a > b) {
        return 1;
    }

    return 0;
} 

//Сортировка курсов
const sortedCoursesByMinCost = (courses) => {
    return courses.sort(compareFunction)
}

//фильтрация по цене
const coursesByUserFilter = (range) => {
    return courses.filter(i => comparePrice(range, i.prices))
}

console.log(sortedCoursesByMinCost(coursesByUserFilter(requiredRange1)));
console.log(sortedCoursesByMinCost(coursesByUserFilter(requiredRange2)));
console.log(sortedCoursesByMinCost(coursesByUserFilter(requiredRange3)));